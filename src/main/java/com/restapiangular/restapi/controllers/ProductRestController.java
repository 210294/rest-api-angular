package com.restapiangular.restapi.controllers;

import com.restapiangular.restapi.entities.ProductEntity;
import com.restapiangular.restapi.repositories.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RequestMapping("/api")
@RestController
public class ProductRestController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/products")
    public List<ProductEntity> getProducts() {
        return this.productRepository.findAll();
    }
    @GetMapping("/products/{id}")
    public ProductEntity getProductById(@PathVariable(name = "id") Integer id) {
        return this.productRepository.findById(id).get();
    }
    @PostMapping("/products")
    public void createProduct(@RequestBody ProductEntity productEntity) {
        this.productRepository.save(productEntity);
    }

    @PutMapping("/products/{id}")
    public void updateProduct(@PathVariable(name = "id") Integer id,
                              @RequestBody ProductEntity productEntity) {
        ProductEntity currentProduct = this.productRepository.findById(id).get();
        currentProduct.setName(productEntity.getName());
        currentProduct.setPrice(productEntity.getPrice());
        currentProduct.setQuantity(productEntity.getQuantity());
        this.productRepository.save(currentProduct);
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable(name = "id") Integer id) {
        this.productRepository.deleteById(id);
    }
}
